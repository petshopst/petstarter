{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}




 {if $page.page_name == 'index'}

 <div class="banner_petshop">
 	<div class="container">
 		<div class="banner_petshop_content">
 			<h2 class="banner_petshop_titulo">Servicio de suscripción de Mi tienda</h2>
 			<p>Suscríbete y ahorra hasta un 20%!</p>
 			<a class="banner_petshop_boton" href="{$urls.pages.index}suscripcion">Más Información</a>
 		</div>
 		<div class="banner_petshop_imagenes">
 				<div class="banner_petshop_imagentop">
 					<img src="{$urls.img_url}banner_spiral_1.png" alt="">
 				</div>
 				<div class="banner_petshop_imagenboton">
 				 <img src="{$urls.img_url}banner_spiral_2.png" alt="">
 				</div>
 		</div>
 	</div>
 </div>
 <div class="selector_petshop">
 	<div class="container">
 		<div class="selector_petshop_content">
 		<a href="{$urls.current_url}3-perros">
      <div class="selector_petshop_perro">
 				<img src="{$urls.img_url}perro.png" alt="">
 				<h3>Perro Shop</h3>
 			</div> </a>
 			<a href="{$urls.current_url}4-gatos">
        <div class="selector_petshop_gato">
 				<img src="{$urls.img_url}gato.png" alt="">
 				<h3>Gato Shop</h3>
 			</div> </a>

 		</div>
 		<div class="selector_petshop_contentb">
 			<p>Queremos que tu mascota reciba el mismo cariño que recibe de ti, por eso queremos conocerlos para ofrecerles los productos correctos y en el momento que necesites </p>
 		</div>
 	</div>
 </div>
 <div id="video_petshop_mobil" class="video_petshop">
 		 <div class="container">
        <div class="video_petshop_content" style=" ">
       <iframe class="videopet" width="100%" height="500" src="https://www.youtube.com/embed/ut5l4yTmD_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
         </div>
    </div>
 </div>
 <div id="video_petshop" class="video_petshop">
    <div class="container">
      <video autoplay muted loop id="video_petshop_video">
       <source src="https://qa.minimi.pet/themes/petstarter/assets/img/videopet2.mp4" type="video/mp4">
     </video>
     <div class="video_petshop_content" style=" ">
                 <div class="video_petshop_title">
                     <h3>Conócenos</h3>
                 </div>
                 <div>
                     <img id="boton" onclick="openFullscreen();" class="video_petshop_boton" src="https://qa.minimi.pet/themes/petstarter/assets/img/boton_video.png" alt="">
                 </div>
                 <div class="video_petshop_boton">
                     <h4>Descubre todo lo que tenemos para ti</h4>
                 </div>
    </div>
    </div>
</div>



 <!--<div class="info_petshop">
 	 <div class="container">
 				<div class="info_petshop_content">
 									<div class="info_petshop_titulo">
 													<h2>Hacemos las entregas más fáciles</h2>
 									</div>
 									<div class="info_petshop_caracteristicas">
 												<div class="">
 																	<img class="icono_info" src="{$urls.img_url}icono_shop.png" alt="">
 																	<h4>Recojo en tienda</h4>
 																	<p>Sin costos adicionales, solo compras y recoges en tienda.</p>
 												</div>
 												<div class="">
 																	<img class="icono_info" src="{$urls.img_url}icono_envio.png" alt="">
 																	<h4>Recojo en tienda</h4>
 																	<p>Sin costos adicionales, solo compras y recoges en tienda.</p>
 												</div>
 												<div class="">
 																	<img class="icono_info" src="{$urls.img_url}icono_descuento.png" alt="">
 																	<h4>Recojo en tienda</h4>
 																	<p>Sin costos adicionales, solo compras y recoges en tienda.</p>
 												</div>

 									</div>
 									<div class="petshop_boton black">
 											<a href="info_petshop_cta">Realiza tu Compra</a>
 									</div>
 				</div>

 	 </div>
 </div> -->
 <div class="recomendaciones_petshop">
 	<div class="recomendaciones_petshop_top">
 	 <div class="container">
 				<div class="recomendaciones_petshop_content">
 									<div class="recomendaciones_petshop_destacado">
 											 <img src="{$urls.img_url}foto_destacado.png" alt="">
 											 <div class="recomendaciones_petshop_destacado_box">
 													<h2>Los amigos de minimi :)</h2>
 													 <img class="logocircular" src="{$urls.img_url}logo_minimi_circular.png" alt="">
 											 </div>

 									</div>
 									<div class="recomendaciones_petshop_texto">
 										<p>Tenemos amigos de cuatro patas, amigos de dos patas, amigos que mueven la cola, amigos que es mejor que no muevan la cola, los amigos nos hacen felices.
En minimi tenemos un grupo de amig@s que nos ayudan a elegir los productos, probarlos y darnos las recomendaciones que hacen felices a sus engreid@s, no encantaría que los conozcas!
</p>
 									</div>
 				</div>
 	 </div>
 	 </div>
 	 <div class="recomendaciones_petshop_boton">
 		 <div class="container">

 			 <div class="recomendaciones_petshop_clientes recomendaciones slider">
         <div>
 						 <div class="clientes_petshop_box">
 								 <img src="{$urls.img_url}foto_cliente.jpg" alt="">
 								 <h3>Jaime y Brako</h3>
 								 <p>Collar Mailo</p>
 						 </div>
         </div>
         <div>
 						 <div class="clientes_petshop_box">
 								 <img src="{$urls.img_url}foto_cliente_2.jpg" alt="">
 								 <h3>Paola y Felix</h3>
 								 <p>Comida Wiskat</p>
 						 </div>
        </div>
        <div>
 						 <div class="clientes_petshop_box">
 								 <img src="{$urls.img_url}foto_cliente_3.jpg" alt="">
 								 <h3>Jenny y Lú</h3>
 								 <p>Arnés Amigo</p>
 						 </div>
        </div>
        <div>
 						 <div class="clientes_petshop_box">
 								 <img src="{$urls.img_url}foto_cliente_4.jpg" alt="">
 								 <h3>Alberto y Ramón</h3>
 								 <p>Antipulgas Bayer</p>
 						 </div>
        </div>
        <div>
             <div class="clientes_petshop_box">
                 <img src="{$urls.img_url}foto_cliente_3.jpg" alt="">
                 <h3>Jaime y Brako</h3>
                 <p>Collar Mailo</p>
             </div>
        </div>
        <div>
             <div class="clientes_petshop_box">
                 <img src="{$urls.img_url}foto_cliente.jpg" alt="">
                 <h3>Jaime y Brako</h3>
                 <p>Collar Mailo</p>
             </div>
        </div>
        <div>
             <div class="clientes_petshop_box">
                 <img src="{$urls.img_url}foto_cliente_2.jpg" alt="">
                 <h3>Jaime y Brako</h3>
                 <p>Collar Mailo</p>
             </div>
        </div>
        <div>
             <div class="clientes_petshop_box">
                 <img src="{$urls.img_url}foto_cliente_4.jpg" alt="">
                 <h3>Jaime y Brako</h3>
                 <p>Collar Mailo</p>
             </div>
        </div>
 			 </div>
 		 </div>

 	 </div>
 </div>
 <div class="instagram_petshop">
 	<div class="container">
 			 <div class="instagram_petshop_content">
 					<div class="instagram_petshop_texto">
 								<p>Siempre tenemos ofertas y novedades en nuestras redes, encuéntranos en Instagram, Facebook, Tiktok o Youtube, síguenos y no te las pierdas. Pero si quieres ver lo último de lo último dale una mirada a esta sección.</p>
 					</div>
 					<div class="instagram_petshop_box">
 						<h2>Ofertas y Novedades</h2>
 						<div class="feed_petshop">

 						</div>
 						 <h4>@minimi.petshop</h4>
 					</div>

 			 </div>

 	</div>
 </div>


 {/if}


  {if $page.page_name == 'quienes'}


 {/if}
