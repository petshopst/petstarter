{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
 {extends file='page.tpl'}

{block name='page_header_container'}{/block}

{block name='page_content'}

<div class="preguntas_frecuentes_petshop">
      <div class="container preguntas_frecuentes">
              <div class="preguntas_frecuentes_titulo">
                  <h2>Preguntas frecuentes</h2>
                  <p>Si es tu primera vez, dale una mirada a las preguntas frecuentes y resuelve tus consultas y pregutnas que tengas.</p>
              </div>

              <div class="preguntas_frecuentes_content">
                <div id="accordion-container">
     <h4 class="accordion-header">¿Cómo me suscribo?</h4>
     <div class="accordion-content">
          <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Qué puedo pedir?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Con qué frecuencia puedo programar mis pedidos de suscripción?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Cómo aplico mi descuento de suscripción?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Califico para la entrega gratis?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Hay una cantidad mínima que puedo pedir?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
     <h4 class="accordion-header">¿Cómo cambio mi suscripción?</h4>
     <div class="accordion-content">
            <p>Es fácil: simplemente navegue por el sitio web para encontrar sus productos favoritos. Cuando esté en la página de un producto, tendrá la opción de 'Suscribirse para guardar' y podrá elegir la frecuencia con la que desea volver a pedir el artículo al agregarlo a su cesta. Al finalizar la compra, ingrese a dónde desea que realicemos la entrega y los detalles de pago, guardaremos estos detalles para pedidos futuros, disfrutará de todos los beneficios exclusivos de ser un suscriptor de Minimi Petshop.</p>
     </div>
</div>

              </div>
      </div>
</div>
{/block}
