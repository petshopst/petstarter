{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
  {block name='breadcrumb'}
{if $page.page_name == 'suscription'}

 {else}
<div class="topbanner_petshop">
  <div class="container">
      <div class="topbanner_petshop_content">

          <div class="">
             <h3>Ahorra un 20%</h3>
          </div>
          <div class="topbanner_petshop_texto">
              <p>en tu primera compra y hasta el 5% en tus siguientes compras</p>
          </div>
          <div class="">
            <div class="petshop_boton black">
                <a href="{$urls.pages.index}suscripcion">Más información</a>
            </div>

          </div>

      </div>

  </div>
</div>
 {/if}
  {if $product.show_price || $page.page_name == 'cart' }


  <div class="petshop_miga">
    <div class="container">
      <nav data-depth="{$breadcrumb.count}" class="breadcrumb hidden-sm-down">
         <ol>
           {block name='breadcrumb'}
             {foreach from=$breadcrumb.links item=path name=breadcrumb}
               {block name='breadcrumb_item'}
                 <li>
                   {if not $smarty.foreach.breadcrumb.last}
                     <a href="{$path.url}"><span>{$path.title}</span></a>
                   {else}
                     <span>{$path.title}</span>
                   {/if}
                 </li>
               {/block}
             {/foreach}
           {/block}
         </ol>
       </nav>
    </div>
  </div>


  <div class="petshop_product_titulo">
    <div class="container">
      <div class="titulo_petshop">
      {block name='page_title'}
       <h1>Detalles del producto</h1>
      {/block}
      </div>
    </div>
 </div>
 {/if}

    {/block}


<!--<nav data-depth="{$breadcrumb.count}" class="breadcrumb hidden-sm-down">
  <ol>
    {*block name='breadcrumb'}
      {foreach from=$breadcrumb.links item=path name=breadcrumb}
        {block name='breadcrumb_item'}
          <li>
            {if not $smarty.foreach.breadcrumb.last}
              <a href="{$path.url}"><span>{$path.title}</span></a>
            {else}
              <span>{$path.title}</span>
            {/if}
          </li>
        {/block}
      {/foreach}
    {/block*}
  </ol>
</nav>-->
