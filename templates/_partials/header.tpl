{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}

{block name='header_nav'}
 <!--<nav class="header-nav">
    <div class="container">
      <div class="row">
        <div class="hidden-sm-down">
          <div class="col-md-5 col-xs-12">
            {hook h='displayNav1'}
          </div>
          <div class="col-md-7 right-nav">
              {hook h='displayNav2'}
          </div>
        </div>
        <div class="hidden-md-up text-sm-center mobile">
          <div class="float-xs-left" id="menu-icon">
            <i class="material-icons d-inline">&#xE5D2;</i>
          </div>
          <div class="float-xs-right" id="_mobile_cart"></div>
          <div class="float-xs-right" id="_mobile_user_info"></div>
          <div class="top-logo" id="_mobile_logo"></div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </nav> -->
{/block}

{block name='header_top'}
<div class="header-top fondo-morado">
   <div class="container">
      <!--<div class="row">-->
       <div class="petshop_cabecera">
         <div class="petshop_cabecera_principal">
           <div id="para-mobile" class="para-mobile">
             <a href="javascript:void(0);" class="icon" onclick="openMenu()">
    <img id="me_open" class="menu_mobile_boton" src="{$urls.img_url}bars-solid.svg" alt="">
    <img id="me_close" class="menu_mobile_boton" src="{$urls.img_url}times-solid.svg" alt="">

 </a>
           </div>
          <div class="para-logo">
            {if $shop.logo_details}
              {if $page.page_name == 'index'}
                <h1>
                  {renderLogo} <span>| Petshop</span>
                </h1>
              {else}
                {renderLogo} <span>| Petshop</span>
              {/if}
            {/if}
         </div>

       </div>
       <div class="petshop_cabecera_secundario">
          <div class="para-menu">
            <div class="para-buscador">
              {hook h='displayTop'}
            </div>
            <div class="para-menuprincipal">
              {hook h='displayNav1'}
            </div>


          </div>
          <div class="para-iconoshop">
              {hook h='displayNav2'}
          </div>
      </div>
        </div>
      <!--</div>-->
  </div>
</div>

  {hook h='displayNavFullWidth'}
{/block}

{if $listing.products|count}
<div class="bannercategorias_petshop"   {if !empty($category.image.large.url)} style="background-image: url({$category.image.large.url});"> {/if}

  <img class="left_bannercategorias" src="{$urls.img_url}bannerleft.png" alt="">
    <div class="container">
      <div class="bannercategorias_petshop_content">
        <div class="bannerimagen_petshop">

        </div>
        <div class="bannertexto_petshop">
            <h1>{$category.name}</h1>
        </div>

      </div>

    </div>
   <img class="rigth_bannercategorias" src="{$urls.img_url}bannerrigth.png" alt="">
</div>
{/if}
