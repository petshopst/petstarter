{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='catalog/listing/product-list.tpl'}

{block name='product_list_header'}
    {include file='catalog/_partials/category-header.tpl' listing=$listing category=$category}

      


    <!--<div class="megacategorias_petshop">

        <div class="megacategorias_petshop_content">

            <div class="cat_petshop">
                <div class="cat_petshop_top">
                   <h2>Cuidado e Higiene</h2><span><a href="#">Mostrar todo</a></span>
                </div>
                <div class="cat_petshop_content">
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Seco</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Húmedo</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Medicado</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Cocido</h6>
                      </div>
                  </a>
                </div>

            </div>


        </div>

        <div class="megacategorias_petshop_content">

            <div class="cat_petshop">
                <div class="cat_petshop_top">
                   <h2>Alimento</h2><span><a href="#">Mostrar todo</a></span>
                </div>
                <div class="cat_petshop_content">
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Seco</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Húmedo</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Medicado</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Cocido</h6>
                      </div>
                  </a>
                </div>

            </div>


        </div>

        <div class="megacategorias_petshop_content">

            <div class="cat_petshop">
                <div class="cat_petshop_top">
                   <h2>Accesorios y Otros</h2><span><a href="#">Mostrar todo</a></span>
                </div>
                <div class="cat_petshop_content">
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Hant</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Húmedo</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Medicado</h6>
                      </div>
                  </a>
                  <a class="cat_petshop_box_link" href="#">
                      <div class="cat_petshop_box">
                              <img src="{$urls.img_url}cateimg.png" alt="">
                              <h6>Alimento Cocido</h6>
                      </div>
                  </a>
                </div>

            </div>


        </div>
    </div> -->
{/block}
