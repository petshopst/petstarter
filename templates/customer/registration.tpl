{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='page.tpl'}

{block name='page_content'}
<div class="container">
   <div class="titulo_petshop">
{block name='page_title'}
   <h1>{l s='Create an account' d='Shop.Theme.Customeraccount'}</h1>
{/block}
  </div>

    {block name='register_form_container'}
      {$hook_create_account_top nofilter}

      <div class="registro_petshop">
        <section class="register-form">
        <p>{l s='Already have an account?' d='Shop.Theme.Customeraccount'} <a href="{$urls.pages.authentication}">{l s='Log in instead!' d='Shop.Theme.Customeraccount'}</a></p>
        {render file='customer/_partials/customer-form.tpl' ui=$register_form}
        </section>
      <div class="registro_petshop_box">
         <img src="{$urls.img_url}petregistro.png" alt="">
         <div class="registro_petshop_box_content">
                <h3>Crea tu cuenta</h3>
                <div class="pasos_petshop">
                    <div class="paso_petshop">
                          <img class="paso" src="{$urls.img_url}rapido.png" alt="">
                          <h6>Rápido</h6>
                    </div>
                    <div class="paso_petshop">
                          <img class="paso" src="{$urls.img_url}facil.png" alt="">
                          <h6>Facil</h6>
                    </div>
                    <div class="paso_petshop">
                          <img class="paso" src="{$urls.img_url}gratis.png" alt="">
                          <h6>Gratis</h6>
                    </div>
                </div>
                <p>Podrás acceder a tu historial de ordenes, manejar tus
suscripciones, escribir comentarios y mucho más.</p>
         </div>
         <h3>Obtén descuentos exclusivos :)</h3>
      </div>
      </div>

    {/block}
{/block}
