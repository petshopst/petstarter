{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends 'customer/page.tpl'}



{block name='page_content'}
<div class="container">
   <div class="titulo_petshop">
{block name='page_title'}
    <h1>{l s='Your personal information' d='Shop.Theme.Customeraccount'}</h1>
{/block}

  </div>
  <p>Aqui podrás gestionar tu cuenta: acceder a tu
historial de ordenes,
manejar tus
suscripciones, escribir comentarios y mucho
más</p>

  <div class="petshop_infocliente_top">
     <ul>
         <li><a href="{$urls.pages.identity}">General</a></li>
         <li><a href="{$urls.pages.addresses}">Mis direcciones</a></li>
         <li><a href="{$urls.pages.history}">Historial de Pedidos</a>  </li>
         <li><a href="{$link->getModuleLink('blockwishlist', 'lists', array(), true)|escape:'html':'UTF-8'}">Mi Lista de deseos</a> </li>
    </ul>
  </div>
<div class="petshop_infocliente_content">
  <div class="petshop_mascotasclientebox">
          <div class="petshop_mascotasclientebox_content">
                <h4 class="petshop_titulo">
                  Mi Mascota
                </h3>
                <div class="petshop_foto_up">
                   <img src="{$urls.img_url}uploadpet.png" alt="Upload Foto">
                </div>
                <p>Tu mascota es única y queremos conocerla y atenderla como lo harías tú, ayudanos ingresando sus datos.</p>
                <a id="newpet" class="boton boton_rojo" href="#">Empecemos</a>
          </div>
          <div class="popuppet fancybox-overlay fancybox-overlay-fixed">
              <div class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened" style="position: relative;max-width: 640px;margin: auto;margin-top: 30px;">
                  <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
                    <div class="fancybox-outer">
                      <div class="fancybox-inner" style="overflow: auto; width: 607.667px; height: 571px;">
                           <div class="petshop_mascota_content">
                             <div class="petshop_mascota_controls">
                              <span class="atras_popup"><a href="#" class="" onclick="atraspop();"><img src="{$urls.img_url}ic_atras.svg" alt=""> </a>  </span>
                              <span class="cerrar_popup"> <img src="{$urls.img_url}ic_close.svg" alt=""> </span>
                             </div>
                                    <div class="petshop_mascota_top">
                                          <img src="{$urls.img_url}icono.png" alt="">
                                    </div>
                                    <div class="petshop_mascota_part1">
                                          <h2>¿Qué mascota tienes?</h2>
                                          <div class="escogerpet">
                                            <div class="perro next2" >
                                                <img src="{$urls.img_url}perro.png" alt="">
                                                 <h3 class="escogerpet_titulo" >Perro</h3>
                                            </div>
                                              <div class="gato next2" >
                                                  <img src="{$urls.img_url}gato.png" alt="">
                                                  <h3  class="escogerpet_titulo" >Gato</h3>
                                              </div>
                                          </div>
                                    </div>
                                    <div class="petshop_mascota_part2">
                                          <h2>¿Cuál es el nombre de tu mascota?</h2>
                                          <p>Tambien podrías poner otro apodo que le digas</p>
                                          <div class="form_nombre">
                                            <input type="text" name="" value="" placeholder="Nombre de tu mascota">
                                            <a id="next3" class="boton boton_morado" href="#">Continuar</a>
                                          </div>
                                    </div>
                                    <div class="petshop_mascota_part3">
                                          <h2>Sube una foto de tu mascota</h2>
                                          <p>Sube una foto o elige la que más se parezca a tu mascota.</p>
                                          <div class="grid_fotos">
                                          <a href="#" ><img  id="next4" class="foto_engrid" src="{$urls.img_url}uploadpetmini.png" alt=""></a>
                                             <img class="foto_engrid" src="{$urls.img_url}foto1.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto2.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto3.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto4.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto5.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto6.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto7.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto8.png" alt="">
                                             <img class="foto_engrid" src="{$urls.img_url}foto9.png" alt="">
                                           </div>
                                    </div>
                                    <div class="petshop_mascota_part4">
                                          <h2>¿Cuál es el peso de tu mascota?</h2>
                                          <p>También podrías ingresar un peso aproximado.</p>
                                          <div class="form_peso">
                                            <select name="pesopet" id="pesopet">
                                                <option value="8kg">8 kg</option>
                                                <option value="10kg">10 kg</option>
                                                <option value="12kg">12 kg</option>
                                                <option value="15kg">15 kg</option>
                                              </select>

                                            <a id="next5" class="boton boton_morado" href="#">Continuar</a>
                                          </div>
                                    </div>
                                    <div class="petshop_mascota_part5">
                                          <h2>¿Cuál es el sexo de tu mascota?</h2>
                                          <p>También podrías ingresar un peso aproximado.</p>
                                          <div class="form_sexo" id="next6">

                                            <input type="radio" id="macho" name="sexopet" value="macho">
                                              <label for="macho">Macho</label><br>
                                            <input type="radio" id="hembra" name="sexopet" value="hembra">
                                              <label for="hembra">Hembra</label><br>
                                         </div>
                                    </div>
                                    <div class="petshop_mascota_part6">
                                          <h2>¿Qué día le celebras su cumpleaños?</h2>
                                          <p>Si no lo recuerdas. Escoge una fecha aproximada</p>
                                          <div class="form_cumple">

                                            <input type="date" name="fecha_cumple" value="" placeholder="Cumpleaños(DD/MM/YYYY)" min="1990-01-01" max="2025-12-31">
                                            <a id="next7" class="boton boton_morado" href="#">Continuar</a>
                                          </div>
                                    </div>
                                    <div class="petshop_mascota_part7">
                                          <h2>¡Bienvenido Brako!</h2>
                                          <p>Gracias por compartir con nosotros esta información, lo valoramos mucho y la utilizaremos con
                                            responsabilidad para poder ayudarte y atenderte a ti y a tu mascota con el mayor cariño</p>
                                          <div class="form_terminar">
                                            <a id="petpopupterminar" class="boton boton_morado cerrar_popup" href="#">Terminar</a>
                                         </div>
                                    </div>
                           </div>
                      </div>
                   </div>
                </div>
              </div>
          </div>
   </div>
   <div class="petshop_infoclientebox">
     {render file='customer/_partials/customer-form.tpl' ui=$customer_form}
   </div>
</div>

</div>
{/block}
