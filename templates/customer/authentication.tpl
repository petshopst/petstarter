{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='page.tpl'}



{block name='page_content'}

<div class="container">
   <div class="titulo_petshop">
  {block name='page_title'}
    <h1>{l s='Login or register' d='Shop.Theme.Customeraccount'}</h1>
  {/block}
  </div>
   <div class="login_petshop">
     <div class="boxlogin_petshop">
       {block name='login_form_container'}
       <section class="login-form">
        <h4>¿Ya tienes cuenta?</h4>
        <p>Es bueno tenerte de vuelta.</p>
           {render file='customer/_partials/login-form.tpl' ui=$login_form}
         </section>
         <hr/>
         {block name='display_after_login_form'}
           {hook h='displayCustomerLoginFormAfter'}
         {/block}
        <!-- <div class="no-account">
           <a href="{$urls.pages.register}" data-link-action="display-register-form">
             {l s='No account? Create one here' d='Shop.Theme.Customeraccount'}
           </a>
         </div>-->
       {/block}
        </div>
     <div class="infologin_petshop">
        <div class="infologin_petshop_content">
          <h4>¿Eres nuevo(a) en Minimi?</h4>
          <p>Crear tu cuenta es rápido, fácil y gratis. Podrás acceder a    tu historial de ordenes, manejar tus suscripciones, escribir comentarios y mucho más</p>
       </div>
       <div class="infologin_petshop_botones">
         <a class="info_boton morado" href="{$urls.pages.register}">Crear mi cuenta</a>

       </div>


     </div>

 </div>
 </div>

{/block}
